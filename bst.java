class bst{
    
    Node root;

    private class Node{
    	
    	// These attributes of the Node class will not be sufficient for those attempting the AVL extra credit.
    	// You are free to add extra attributes as you see fit, but do not remove attributes given as it will mess with help code.
        String keyword;
        Record record;
        int size;
        Node l;
        Node r;

        private Node(String k){
        	// TODO Instantialize a new Node with keyword k.
        	this.keyword = k;
        }

        private void update(Record r){
        	//TODO Adds the Record r to the linked list of records. You do not have to check if the record is already in the list.
        	//HINT: Add the Record r to the front of your linked list.
        	r.next = this.record;
        	this.record = r;
        }

       
    }

    public bst(){
        this.root = null;
    }
      
    public void insert(String keyword, FileData fd){
    	Record recordToAdd = new Record(fd.id, fd.title, fd.author, null);
        //TODO Write a recursive insertion that adds recordToAdd to the list of records for the node associated
        //with keyword. If there is no node, this code should add the node.
    	root = insert(keyword, root, recordToAdd);
    }
    
    //returns a Node
    //takes a String keyword, 
    //Node n (will start with root to start recursive run),
    //and Record recordToAdd
    private Node insert(String keyword, Node n, Record recordToAdd){
    	
    	//Node with keyword was not found, create a new node and add the record to it
    	//This will only happen if we are at the appropriate leaf position
    	if(n == null){
    		Node newNode = new Node(keyword);
    		newNode.update(recordToAdd);
    		return newNode;
    	}
    	
    	int comparison = keyword.compareTo(n.keyword);
    	
    	if(comparison < 0){ //keyword is alphabetically before current node
    		n.l = insert(keyword, n.l, recordToAdd);
    	}else if(comparison > 0){ //keyword is alphabetically after current node
    		n.r = insert(keyword, n.r, recordToAdd);
    	}else{ //found the keyword we want, insert record here
    		n.update(recordToAdd);
    	}
    	return n; //runs if we find the keyword, will end up setting a node to itself
    }
    
    public boolean contains(String keyword){
    	//TODO Write a recursive function which returns true if a particular keyword exists in the bst
    	return findNode(keyword, root) != null; //find node starting recursive run at root
    }
    
    //Finds a node with specified keyword
    //takes String keyword,
    //and Node n (will start with root to start recursive run)
    private Node findNode(String keyword, Node n){
    	
    	//Node with keyword was not found
    	if(n == null){
    		return null;
    	}
    	
    	int comparison = keyword.compareTo(n.keyword);
    	
    	if(comparison < 0){ //keyword is alphabetically before current node 
    		return findNode(keyword, n.l);
    	}else if(comparison > 0){ //keyword is alphabetically after current node
    		return findNode(keyword, n.r);
    	}else{ //found node
    		return n;
    	}
	}

    public Record get_records(String keyword){
        //TODO Returns the first record for a particular keyword. This record will link to other records
    	//If the keyword is not in the bst, it should return null.
    	Node n =  findNode(keyword, root); //find node, start recursive run at root
    	if(n != null){ //return record if the keyword exists
    		return n.record;
    	}
    	return null; //return null otherwise
    }

    public void delete(String keyword){
    	//TODO Write a recursive function which removes the Node with keyword from the binary search tree.
    	//You may not use lazy deletion and if the keyword is not in the bst, the function should do nothing.
    	root = delete(keyword, root); //delete node, start recursive run at root
    }
    
    //deletes a node
    private Node delete(String keyword, Node n){
    	
    	//Node could not be found, cannot delete
    	if(n == null){
    		return n;
    	}
    	
    	int comparison = keyword.compareTo(n.keyword);
    	
    	if(comparison < 0){ //keyword is alphabetically before current node
    		n.l = delete(keyword, n.l);
    	}else if(comparison > 0){ //keyword is alphabetically after current node
    		n.r = delete(keyword, n.r);
    	}else if(n.l != null && n.r != null){ //Node found, two children case
    		//get lowest node in right subtree
    		Node lowestNode = findMin(n.r);
    		
    		//set this node, the one to delete, to have lowest right subtree node's properties
    		n.keyword = lowestNode.keyword;
    		n.record = lowestNode.record;
    		
    		//delete the lowest right subtree node, the one we duplicated up to here
    		n.r = delete(n.keyword, n.r);
    	}else{ //one child or no children case
    		//set this node, the one we are deleting, to the existing single child
    		n = (n.l != null) ? n.l : n.r;
    	}
    	return n;
    }
    
    //find largest child of this node
    //finds itself if no larger children
    private Node findMax(Node n){
    	if(n != null){
    		while(n.r != null){
    			n = n.r;
    		}
    	}
    	return n;
    }
    
    //find smallest child of this node
    //finds itself if no smaller children
    private Node findMin(Node n){
    	if(n != null){
    		while(n.l != null){
    			n = n.l;
    		}
    	}
    	return n;
    }

    public void print(){
        print(root);
    }

    private void print(Node t){
        if (t!=null){
            print(t.l);
            System.out.println(t.keyword);
            Record r = t.record;
            while(r != null){
                System.out.printf("\t%s\n",r.title);
                r = r.next;
            }
            print(t.r);
        } 
    }
    

}
